"""this is our test for get_request"""
# import os
# import random
# import re
# import subprocess
# import string
from pyblog import get_request, file_blog_method

def test_get_request_success(mocker):
    mock_get = mocker.patch('requests.get')
    mock_response = mock_get.return_value
    mock_response.status_code = 200
    mock_response.json.return_value = [{
    'author': 2,
    'categories': [1],
    'comment_status': 'open',
    'content': {'protected': False,
                'rendered': '<p>testcontent1<br />\ntestcontent2</p>\n'},
    'date': '2020-12-17T15:13:58',
    'date_gmt': '2020-12-17T15:13:58',
    'excerpt': {'protected': False,
                'rendered': '<p>testcontent1 testcontent2</p>\n'},
    'featured_media': 0,
    'format': 'standard',
    'guid': {'rendered': 'http://3.21.12.252:8088/2020/12/17/test-title-14/'},
    'id': 323,
    'link': 'http://3.21.12.252:8088/2020/12/17/test-title-14/',
    'meta': [],
    'modified': '2020-12-17T15:13:58',
    'modified_gmt': '2020-12-17T15:13:58',
    'ping_status': 'open',
    'slug': 'test-title-14',
    'status': 'publish',
    'sticky': False,
    'tags': [],
    'template': '',
    'title': {'rendered': 'test title'},
    'type': 'post'},]
    
    date_g, title_g, content_g = get_request()
    
    assert title_g == 'test title'
    assert content_g == '<p>testcontent1<br />\ntestcontent2</p>\n'
    assert date_g == '2020-12-17T15:13:58'


# --------------------------------------------------
def test_get_request_not_success(mocker):
    mock_get = mocker.patch('requests.get')
    mock_response = mock_get.return_value
    mock_response.status_code != 200
    mock_response.json.return_value = None
    
    date_g, title_g, content_g = get_request()
    
    assert title_g == None
    assert content_g == None
    assert date_g == None


# --------------------------------------------------
# def def_file_blog_method(mocker):
#     mock_get = mocker.patch('requests.get')
#     mock_response = mock_get.return_value
#     mock_response.status_code = 201
#     mock_response.json.return_value = {
#             'date': '2020-12-17T15:13:58',
#             'title': 'test title',
#             'content': '<p>testcontent1<br />\ntestcontent2</p>\n',
#             'status': 'publish',
#         }

#     result = file_blog_method()

#     assert result == 'Success'

# # --------------------------------------------------
# def test_exists():
#     """files exists"""

#     assert os.path.isfile('./pyblog.py')
#     assert os.path.isfile('./latin')
# --------------------------------------------------
