#!/usr/bin/env python3

import requests
import json
import base64
import argparse
import sys
#import fileinput
from datetime import datetime


current_time = datetime.now().isoformat().split('.')

url = 'http://3.21.12.252:8088/wp-json/wp/v2'

user = 'eamonn'
password = 'nsvY XMSM rB4D wLgP tWfg Iqp2'

creds = user + ':' + password

token = base64.b64encode(creds.encode())

header = {'Authorization': 'Basic ' + token.decode('utf-8')}


def get_args():
    """ Get command-line arguments """

    #Parser will figure out all the arguments
    parser = argparse.ArgumentParser(description='Request type for blog pose') 
    # Parser will except positional argument for get or post blog request
    parser.add_argument('string', type=str, help='It is used for read(get) or upload(post) requests', choices=['read','upload'])
    # Parser will except optinal filename argument for a post blog request
    parser.add_argument('-f','--filename', action='store', type=argparse.FileType('r'), help='Input filename')

    # Parse any argument to the program
    return parser.parse_args() 

def get_request():
    try:
        r = requests.get(url + '/posts', headers=header)
        i = r.json()[0]
        response = [i['date'], i['title'], i['content']]
        date_g, title_g, content_g = response[0], response[1]['rendered'], response[2]['rendered']
        
    except Exception as e:
        print("Please ensure you are connected to the internet and try again.")
        print(e)

    #dictionary = json.dumps(response, sort_keys = True, indent = 4)
    #print(dictionary)
    
    return date_g, title_g, content_g

def post_request():
    try:
        post = {
            'date': current_time[0],
            'title': 'attemp 12',
            'content': 'This is the upload attempt #12',
            'status': 'publish'
        }
    
        r = requests.post(url + '/posts', headers=header, json=post)

    except Exception as e:
        r = "Please ensure you are connected to the internet and try again." + e

    return r

def blog_method():
    args = get_args()
    if args.string == "read":
        get_request()

    if args.string == "upload":
        file_blog_method()


def file_blog_method():
    args = get_args()
#    with open(args.filename, 'r') as f:
    title = args.filename.readline()
    content = args.filename.read()
    if True:
        try:
            payload = {
                'date': current_time[0],
                'title': title,
                'content': content,
                'status': 'publish',
            }
            
            r = requests.post(url + '/posts', headers=header, json=payload)
    
        except Exception as e:
            print("Please ensure you are connected to the internet and try again.")
            print(e)

        print(get_request())

def print_output(date_g, title_g, content_g):
    """ Prints pyblog content.
    Parameters:
    """
    print(f'date:   {date_g}')
    print(f'title: {title_g}\n')
    print(f'content: {content_g}')


if __name__ == "__main__":
    blog_method()
    date_g, title_g, content_g = get_request()
    print_output(date_g, title_g, content_g)
