#!/usr/bin/env python3

import requests
import json
import base64

url = 'http://3.21.12.252:8088/wp-json/wp/v2'

user = 'eamonn'
password = 'nsvY XMSM rB4D wLgP tWfg Iqp2'

creds = user + ':' + password

token = base64.b64encode(creds.encode())

header = {'Authorization': 'Basic ' + token.decode('utf-8')}

post = {
    'date': '2019-09-21T10:00:00',
    'title': 'First API Post',
    'content': 'This is our first python wordpress api post!',
    'status': 'publish'
}

r = requests.post(url + '/posts', headers=header, json=post)

print(r)
