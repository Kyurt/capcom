#!/usr/bin/env python3

import requests
import json
import base64
from datetime import date

user = 'eamonn'
password = 'nsvY XMSM rB4D wLgP tWfg Iqp2'
url = 'http://3.21.12.252:8088/wp-json/wp/v2'

#current_date = date.today()
#post_title = 
#post_content =

creds = user + ':' + password
token = base64.b64encode(creds.encode())
header = {'Authorization': 'Basic ' + token.decode('utf-8')}

def publish_post():

    post = {
        'date': '2019-09-21T10:00:00',
        'title': 'some API Post',
        'content': 'This is our first python wordpress api post!',
        'status': 'publish'
    }

    r = requests.post(url + '/posts', headers=header, json=post)
    print('Your post is published on ' + json.loads(r.content)['link'])
    print(r)
    #return (r)
    dictionary = json.dumps(r.json(), sort_keys = True, indent = 4)
    print(dictionary)

publish_post()
