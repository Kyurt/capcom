#!/usr/bin/env python3

import requests
import json
import base64
import argparse
import sys
from datetime import datetime

def get_args():
    """ Get command-line argumets """

    # Parser will figure out all the arguments
    parser = argparse.ArgumentParser(description='Request type for blog pose') 
    # Parser will except positional argument for get or post blog request
    parser.add_argument('string', type=str, help='It is used for read(get) or upload(post) requests', choices=['read','upload',], default='read')



#    parser.add_argument('-f','--filename', metavar='FILE' ,action='store', default=[sys.stdin], help='Input file(s)')

    # Parse any argument to the program
    return parser.parse_args() 


current_time = datetime.now().isoformat().split('.')

url = 'http://3.21.12.252:8088/wp-json/wp/v2'

user = 'eamonn'
password = 'nsvY XMSM rB4D wLgP tWfg Iqp2'

creds = user + ':' + password

token = base64.b64encode(creds.encode())

header = {'Authorization': 'Basic ' + token.decode('utf-8')}

def blog_method():
    args = get_args()
    print(args)
    if args.string == "read":
        try:
            r = requests.get(url + '/posts', headers=header)
            for i in r.json():
                print(i['date'], i['title'], i['content'])
                break
        except Exception as e:
            print("Please ensure you are connected to the internet and try again.")
            print(e)

    if args.string == "upload":
        try:
            post = {
                'date': current_time[0],
                'title': 'filename4',
                'content': 'This is the attempt #4',
                'status': 'publish'
            }
            
            r = requests.post(url + '/posts', headers=header, json=post)
       
        except Exception as e:
            print("Please ensure you are connected to the internet and try again.")
            print(e)


if __name__ == "__main__":
    print(get_args())
    blog_method()
