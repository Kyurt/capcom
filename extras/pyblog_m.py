#!/usr/bin/env python3

# import json
import base64
import argparse
import sys
from datetime import datetime
import requests
import os

current_time = datetime.now().isoformat().split('.')

# configurations
user = os.environ.get('WORDPRESS_USERNAME', 'eamonn')
passwrd = os.environ.get('WORDPRESS_PASSWORD', 'nsvY XMSM rB4D wLgP tWfg Iqp2')
url = os.environ.get('WORDPRESS_URL', 'http://3.21.12.252:8088/wp-json/wp/v2')

# url = 'http://3.21.12.252:8088/wp-json/wp/v2'
# url = 'http://' + IP + ':8088/wp-json/wp/v2'
# user = 'eamonn'
# password = 'nsvY XMSM rB4D wLgP tWfg Iqp2'

creds = user + ':' + passwrd
token = base64.b64encode(creds.encode())

header = {'Authorization': 'Basic ' + token.decode('utf-8')}


def get_args():
    """ Get command-line arguments """

    parser = argparse.ArgumentParser(description='Request type for blog post')
    parser.add_argument('string',
                        type=str,
                        choices=['read', 'upload'],
                        const='read',
                        nargs='?',
                        help='read(get) or upload(post) requests',
                        default='read')
    parser.add_argument('-f', '--filename',
                        type=str,
                        help='filename')

    return parser.parse_args()


def get_request():
    """ This is the get_request function for the wordpress"""
    try:
        r = requests.get(url + '/posts', headers=header)
        if r.status_code == 200:
            i = r.json()[0]
            response = [i['date'], i['title'], i['content']]
            date_g = response[0]
            title_g = response[1]['rendered']
            content_g = response[2]['rendered']
            # elif r.status_code == 404:
            #     return 'Not Found'
            # elif r.status_code == 500:
            #     return 'Server Error'
            # else:
            #     return 'Response ' + str(r.status_code)
        else: 
            date_g = None
            title_g = None
            content_g = None

    except Exception as e:
        print("Please ensure you are connected to the internet and try again.")
        print(e)

    return date_g, title_g, content_g


def blog_method():
    """ this is the blog_method for read or upload """
    args = get_args()
    if args.string == "read":
        get_request()

    if args.string == "upload":
        file_blog_method()


def file_blog_method():
    """ this is upload function for wordpress blog"""
    args = get_args()
    if args.filename == "-":
        title = sys.stdin.readline()
        content = sys.stdin.read()
    else:
        with open(args.filename, 'r') as f:
            title = f.readline()
            content = f.read()
    try:
        payload = {
            'date': current_time[0],
            'title': title,
            'content': content,
            'status': 'publish',
        }

        response = requests.post(url + '/posts', headers=header, json=payload)

    except Exception as e:
        print("Please ensure you are connected to the internet")
        print(e)

    get_request()
    print(response.status_code)
    if response.status_code == 201:
        return 'Success'
    elif response.status_code == 404:
        return 'Not Found'
    elif response.status_code == 500:
        return 'Server Error'
    else:
        return 'Response ' + str(response.status_code)


def print_output(date_g, title_g, content_g):
    """ Prints pyblog content. """
    print(f'Blog Date:    {date_g:16}')
    print(f'Blog Title:   {title_g:16}\n')
    print(f'Blog Content: {content_g:16}')


if __name__ == "__main__":
    blog_method()
    date_g, title_g, content_g = get_request()
    print_output(date_g, title_g, content_g)
